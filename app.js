var port = process.env.PORT || 9090,
  http = require('http')

var server = http.createServer(function (req, res) {
  var os = require("os")
  res.writeHead(200);
  res.write('Hello from ' + os.hostname());
  res.end();
});

server.listen(port, function () {
  console.log('Example app listening on port ' + port)
});